const fspromise=require('node:fs').promises

async function iterate(){
    let main_data = await fspromise.readFile('./index.json','utf-8')
    main_data = JSON.parse(main_data)

    let directory_path = main_data.topics.map((each)=>{
        return each.directory_path
    })
    const topics=[]

    for(let topic of directory_path){
        let topic_data = await fspromise.readFile(`./${topic}/index.json`,'utf-8')
        topic_data = JSON.parse(topic_data)
       
        let content_arr = []
        for(let part of topic_data.parts){
            let obj = {}
            obj['name'] = part.name
            let parts_data = await fspromise.readFile(`${part.file_location}`,'utf-8')
            obj['content'] = parts_data
            content_arr.push(obj)
        }
        topic_data.parts = content_arr

        topics.push(topic_data)
    }
    let final_result = {}

    final_result["name"] = "Full Stack JavaScript"

    final_result['topics'] = topics

    await fspromise.writeFile('./output.json',JSON.stringify(final_result))
}
iterate()